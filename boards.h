#ifndef BOARDS_H
#define BOARDS_H

#define BOARD_UNKNOWN -1

#define BOARD_RAMPS_OLD         3    // MEGA/RAMPS up to 1.2
#define BOARD_RAMPS_13_EFB      33   // RAMPS 1.3 (Power outputs: Extruder, Fan, Bed)
#define BOARD_RAMPS_13_EEB      34   // RAMPS 1.3 (Power outputs: Extruder0, Extruder1, Bed)
#define BOARD_RAMPS_13_EFF      35   // RAMPS 1.3 (Power outputs: Extruder, Fan, Fan)
#define BOARD_RAMPS_13_EEF      36   // RAMPS 1.3 (Power outputs: Extruder0, Extruder1, Fan)
#define BOARD_RAMPS_13_SF       38   // RAMPS 1.3 (Power outputs: Spindle, Controller Fan)
#define BOARD_RAMPS_14_EFB      43   // RAMPS 1.4 (Power outputs: Extruder, Fan, Bed)
#define BOARD_RAMPS_14_EEB      44   // RAMPS 1.4 (Power outputs: Extruder0, Extruder1, Bed)
#define BOARD_RAMPS_14_EFF      45   // RAMPS 1.4 (Power outputs: Extruder, Fan, Fan)
#define BOARD_RAMPS_14_EEF      46   // RAMPS 1.4 (Power outputs: Extruder0, Extruder1, Fan)
#define BOARD_RAMPS_14_SF       48   // RAMPS 1.4 (Power outputs: Spindle, Controller Fan)
#define BOARD_MELZI             63   // Melzi
#define BOARD_MELZI_MAKR3D      66   // Melzi with ATmega1284 (MaKr3d version)
#define BOARD_ULTIMAKER         7    // Ultimaker
#define BOARD_ULTIMAKER_OLD     71   // Ultimaker (Older electronics. Pre 1.5.4. This is rare)
#define BOARD_ULTIMAIN_2        72   // Ultimainboard 2.x (Uses TEMP_SENSOR 20)
#define BOARD_RUMBA             80   // Rumba
#define BOARD_RAMBO             301  // Rambo
#define BOARD_MINIRAMBO         302  // Mini-Rambo
#define BOARD_MKS_BASE          40   // MKS BASE 1.0

#define BOARD_99                99   // This is in pins.h but...?
#define BOARD_RSS               100  // RSS modified MR1.4
#define BOARD_MKSRSS            101  // RSS modified MKS gen1.4
#define BOARD_MPX3              102  // MPX RSS version

#define MB(board) (MOTHERBOARD==BOARD_##board)

#endif //__BOARDS_H
