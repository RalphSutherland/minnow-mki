/**
 * RSS RAMPS v1.4 pin assignments 
 * EEB + tiny fan on pin 11 & 6, z probe on 5 
 *
 *  ie RAMPS_14_EEBFF (Extruder, Extruder, Bed, Fan, Fan )
 */
#define IS_RAMPS_EEB

#include "pins_RAMPS_14.h"

#ifdef FAN_PIN
#undef FAN_PIN
#endif
#define FAN_PIN            6 // servo01 pin on tinyfan

#ifdef Z_MIN_PROBE_PIN
#undef Z_MIN_PROBE_PIN
#endif
#define Z_MIN_PROBE_PIN    5  //  servo pin 5 with +5 V enabled
//
//
// override  Extruder cooling fans and enable whether or not in config advanced
// Configure fan pin outputs to automatically turn on/off when the associated
// extruder temperature is above/below EXTRUDER_AUTO_FAN_TEMPERATURE.
// Multiple extruders can be assigned to the same pin in which case
// the fan will turn on when any selected extruder is above the threshold.

#ifdef EXTRUDER_0_AUTO_FAN_PIN
#undef EXTRUDER_0_AUTO_FAN_PIN
#endif
#ifdef EXTRUDER_1_AUTO_FAN_PIN
#undef EXTRUDER_1_AUTO_FAN_PIN
#endif
#ifdef EXTRUDER_2_AUTO_FAN_PIN
#undef EXTRUDER_2_AUTO_FAN_PIN
#endif
#ifdef EXTRUDER_3_AUTO_FAN_PIN
#undef EXTRUDER_3_AUTO_FAN_PIN
#endif

#define EXTRUDER_0_AUTO_FAN_PIN 11  // servo0 pin aux1-driving tinyfan
#define EXTRUDER_1_AUTO_FAN_PIN 11
#define EXTRUDER_2_AUTO_FAN_PIN -1
#define EXTRUDER_3_AUTO_FAN_PIN -1

#ifdef EXTRUDER_AUTO_FAN_TEMPERATURE
#undef EXTRUDER_AUTO_FAN_TEMPERATURE
#endif
#ifdef EXTRUDER_AUTO_FAN_SPEED
#undef EXTRUDER_AUTO_FAN_SPEED
#endif

#define EXTRUDER_AUTO_FAN_TEMPERATURE 50
#define EXTRUDER_AUTO_FAN_SPEED   255  // == full speed


